Tom's thesis Notes:
**Page i**
Because of this ECG analysis --> Because of this, ECG analysis
Because of the fundamental importance --> Due to the fundamental importance (started 2 consecutive sentences with "Because of")
solely focusses on --> solely focuses on
Consequently knowing the --> Consequently, knowing the
Baseline wander noise, has been --> Baseline wander noise has been (remove comma)
Of the algorithms investigated the digital filtering --> Of the algorithms, investigated the digital filtering
performance rapidly degrades with increases (in) noise

**Page 1**
(ECG) signal and it�s physical --> (ECG) signal and its physical
ST-T segment --> ST segment

**Page 2**
The QRS complex is also, often used (remove the comma after also)

**Page 3**
1.4 Importance of work - final paragraph of this section is incomplete

1.5 QRS complex detectors will [be] detailed

**Page 4**
causes the ECG and it�s characteristic shape. --> causes the ECG's characteristic shape.

The contraction of the atria is represented in the ECG by the P wave, with the ventricles being represented by the QRS
complex --> The depolarisation through the atrial tissue is represented in the ECG by the P wave and gives rise to
atrial contraction, while the ventricular depolarisation is represented by the QRS complex and causes ventricular contraction.

**Page 5**
For example if two patients were presented with the same cardiac --> For example, if two patients presented with the same cardiac (add comma, take away "were")

Alternatively, if two patients were presented with the same ECG condition --> take away "were"

**Page 6**
Commonly, the (add comma)

are concerned with robust of detection (take away "of")

As with the recording of all physical signals undesirable artifacts appear, which in the case of
the ECG adversely affect it�s utility as a tool in medical diagnosis 
--> As with the recording of all physical signals, undesirable artifacts appear. In the case of
the ECG, this adversely affects its utility as a tool in medical diagnosis

Algorithms, which analyse (remove comma)
(higher level) manner, also require (remove comma)

As with the recording of all physical signals undesirable artifacts appear, which in the case of
the ECG adversely affect it�s utility as a tool in medical diagnosis 
--> As with the recording of all physical signals, undesirable artifacts appear. In the case of
the ECG, this adversely affects its utility as a tool in medical diagnosis (repeated)

In an ECG recording typical high-frequency --> In an ECG recording, typical high frequency (add comma)

noise sources that degrade an ECG signal is: ("are" not "is")

**Page 7**
Electrode contact and motion noise consists is caused by intermittent --> Electrode contact and motion noise interference is caused by intermittent

motion noise overlaps withthe frequencies (add space between "with" and "the")

As such this form of noise (add comma after "such")

adversely effect the performance (should be affect not effect)

This form of noise is usually contains (remove "is")

**Page 8**
It is also, not uncommon (remove comma)
In some implementations this is of the form (add comma after "implementations")

**Page 9**
or processing of, large datasets (remove comma)

**Page 12**
In ECG Signal analysis some plausible states --> In ECG signal analysis, some plausible states (lower case S, add comma)
Consequently to acquire a useful crosssection --> Consequently, to acquire a useful crosssection (add comma)

**Page 13**
all the software included in available in source --> all the software included is available in source ("is" not "in")
