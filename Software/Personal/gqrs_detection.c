void gqrs(WFDB_Time from, WFDB_Time to)
{
    int c, i, qamp, q0, q1 = 0, q2 = 0, rr, rrd, rt, rtd, rtdmin;
    struct peak *p, *q = NULL, *r = NULL, *tw;
    WFDB_Time last_peak = from, last_qrs = from;

    while (t <= to + sps) {
	if (countdown < 0 && sample_valid())
	    qf();
	else if (countdown < 0) {
	    countdown = strtim("1");
	    state = CLEANUP;
	}
	else if (countdown-- <= 0)
	    break;

	q0 = q(t); q1 = q(t-1); q2 = q(t-2);
	if (q1 > pthr && q2 < q1 && q1 >= q0 && t > dt4) {
	    addpeak(t-1, q1);
	    last_peak = t-1;
	    for (p = cpeak->next; p->time < t - rtmax; p = p->next) {
		if (p->time >= annot.time + rrmin && peaktype(p) == 1) {
		    if (p->amp > qthr) {
			rr = p->time - annot.time;
			if (rr > rrmean + 2 * rrdev &&
			    rr > 2 * (rrmean - rrdev) &&
			    (q = find_missing(r, p))) {
			    p = q;
			    rr = p->time - annot.time;
			    annot.subtyp = 1;
			}
			if ((rrd = rr - rrmean) < 0) rrd = -rrd;
			rrdev += (rrd - rrdev) >> 3;
			if (rrd > rrinc) rrd = rrinc;
			if (rr > rrmean) rrmean += rrd;
			else rrmean -= rrd;
			if (p->amp > qthr * 4) qthr++;
			else if (p->amp < qthr) qthr--;
			if (qthr > pthr * 20) qthr = pthr * 20;
			last_qrs = p->time;
			if (state == RUNNING) {
			    int qsize;

			    annot.time = p->time - dt2;
			    annot.anntyp = NORMAL;
			    annot.chan = sig;
			    qsize = p->amp * 10.0 / qthr;
			    if (qsize > 127) qsize = 127;
			    annot.num = qsize;
			    putann(0, &annot);
			    annot.time += dt2;
			}
			/* look for this beat's T-wave */
			tw = NULL; rtdmin = rtmean;
			for (q = p->next; q->time > annot.time; q = q->next) {
			    rt = q->time - annot.time - dt2;
			    if (rt < rtmin) continue;
			    if (rt > rtmax) break;
			    if ((rtd = rt - rtmean) < 0) rtd = -rtd;
			    if (rtd < rtdmin) {
				rtdmin = rtd;
				tw = q;
			    }
			}
			if (tw) {
			    static WFDB_Annotation tann;

			    tann.time = tw->time - dt2;
			    if (debug && state == RUNNING) {
				tann.anntyp = TWAVE;
				tann.chan = sig+1;
				tann.num = rtdmin;
				tann.subtyp = (tann.time > annot.time + rtmean);
				tann.aux = NULL;
				putann(0, &tann);
			    }
			    rt = tann.time - annot.time;
			    if ((rtmean += (rt - rtmean) >> 4) > rtmax)
				rtmean = rtmax;
			    else if (rtmean < rtmin)
				rtmean = rrmin;
			    tw->type = 2;	/* mark T-wave as secondary */
			}
			r = p; q = NULL; qamp = 0; annot.subtyp = 0;
		    }
		    else if (t - last_qrs > rrmax && qthr > qthmin)
			qthr -= (qthr >> 4);
		}
	    }
	}
	else if (t - last_peak > rrmax && pthr > pthmin)
	    pthr -= (pthr >> 4);

	if (++t >= next_minute) {
	    next_minute += spm;
	    (void)fprintf(stderr, ".");
	    (void)fflush(stderr);
	    if (++minutes >= 60) {
		(void)fprintf(stderr, " %s\n", timstr(-t));
		minutes = 0;
	    }
	}
    }

    if (state == LEARNING)
	return;
    
    /* Mark the last beat or two. */
    for (p = cpeak->next; p->time < (p->next)->time; p = p->next) {
	//	if (to > 0 && p->time > to - sps)
	//    break;
	if (p->time >= annot.time + rrmin && p->time < tf && peaktype(p) == 1) {
	    annot.anntyp = NORMAL;
	    annot.chan = sig;
	    annot.time = p->time;
	    putann(0, &annot);
	}
    }
}
