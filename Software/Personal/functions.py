def qrs_compare_extraction(algorithm, record_number): #returns (TP, TN, FP, FN) values from the appropriate file
	
	#algorithm = 'gqrs'
	#record_number = '118e_6' 
	record = './results/' + algorithm + '_' + record_number + '.txt'
	#print(record)
	with open(record,'r') as f:
		read_data = f.readlines()
	TP = 0
	TN = 0
	FP = 0
	FN = 0
	#Se = 0
	#Pp = 0
	#for i in range(7,13):
		#print(read_data[i], i)
	for i in range(7,8+1):
		TP += int(read_data[i][4:9])
	for i in range(11,11+1):
		FP += int(read_data[i][4:9])
	for i in range(7,8+1):
		FN += int(read_data[i][24:29])
	#Se = (TP / (TP + FN) )
	#Pp = (TP / (TP + FP) )
	#print(TP, TN, FP, FN)
	#print(TP, TN, FP, FN, Se, Pp)
	return(algorithm, record_number, TP, TN, FP, FN)
