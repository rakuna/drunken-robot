WFDB_Sample ltsamp(WFDB_Time t)
{
    int dy;
    static int Yn, Yn1, Yn2;
    static WFDB_Time tt = (WFDB_Time)-1L;

    if (lbuf == NULL) {
	lbuf = (WFDB_Sample *)malloc((unsigned)BUFLN*sizeof(WFDB_Sample));
	ebuf = (int *)malloc((unsigned)BUFLN * sizeof(int));
	if (lbuf && ebuf) {
	    for (ebuf[0] = sqrt(lfsc), tt = 1L; tt < BUFLN; tt++)
		ebuf[tt] = ebuf[0];
	    if (t > BUFLN) tt = (WFDB_Time)(t - BUFLN);
	    else tt = (WFDB_Time)-1L;
	    Yn = Yn1 = Yn2 = 0;
	}
	else {
	    (void)fprintf(stderr, "%s: insufficient memory\n", pname);
	    exit(2);
	}
    }
    if (t < tt - BUFLN) {
        fprintf(stderr, "%s: ltsamp buffer too short\n", pname);
	exit(2);
    }
    while (t > tt) {
	static int aet = 0, et;
	WFDB_Sample v0, v1, v2;

	Yn2 = Yn1;
	Yn1 = Yn;
	if ((v0 = sample(sig, tt)) != WFDB_INVALID_SAMPLE &&
	    (v1 = sample(sig, tt-LPn)) != WFDB_INVALID_SAMPLE &&
	    (v2 = sample(sig, tt-LP2n)) != WFDB_INVALID_SAMPLE)
	    Yn = 2*Yn1 - Yn2 + v0 - 2*v1 + v2;
	dy = (Yn - Yn1) / LP2n;		/* lowpass derivative of input */
	et = ebuf[(++tt)&(BUFLN-1)] = sqrt(lfsc +dy*dy); /* length transform */
	lbuf[(tt)&(BUFLN-1)] = aet += et - ebuf[(tt-LTwindow)&(BUFLN-1)];
	/* lbuf contains the average of the length-transformed samples over
	   the interval from tt-LTwindow+1 to tt */
    }
    return (lbuf[t&(BUFLN-1)]);
}
