import functions

output_data = open('results.txt','w')

results = []
i_n = 0
for i in ('gqrs', 'wqrs', 'sqrs', 'sqrs125'):
	print(i)
	for j in ('118', '119'):
		for k in ('e_6', 'e00', 'e06', 'e12', 'e18', 'e24'):
			(a,b,c,d,e,f) = functions.qrs_compare_extraction(i, j+k)
			#print(a,b,c,d,e,f)
			output_data.write(a + ', ' + b + ', ' + str(c) + ', ' + str(d) + ', ' + str(e) + ', ' + str(f) + '\n')
(algorithm, record, TP, TN, FP, FN) = functions.qrs_compare_extraction('gqrs','118e_6')

